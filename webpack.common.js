const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const DIST = path.resolve(__dirname, "dist");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: DIST,
    filename: "bundle.js",
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Überholabstand",
    }),
    new CleanWebpackPlugin(),
  ],
};
